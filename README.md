# La TeXiothèque de lmdbt.fr

## Présentation

[**La TeXiothèque**](https://lmdbt.forge.apps.education.fr/latexiotheque) est un espace dédié au partage de mes ressources pédagogiques conçues avec soin pour mon espace web [lmdbt.fr](https://lmdbt.fr). Vous y trouverez :
- Des fiches d'activités (format PDF et .tex)
- Des fiches de leçon (format PDF et .tex)
- Des fiches d'exercices (format PDF et .tex)
- Des dialogues entre personnages (format PDF et LibreOffice Impress)

Cependant, veuillez noter que mes évaluations blanches (dites "non-notées") et mes évaluations ne sont pas disponibles ici. 

J'ambitionne également de transposer en `.tex` certains automatismes rédigés initialement sur LibreOffice Calc.

## Licence

Les documents et illustrations de **La TeXiothèque** sont mis à disposition sous licence [Creative Commons Attribution (CC-BY)](https://creativecommons.org/licenses/by/4.0/). Vous êtes encouragés à les utiliser, tout en créditant leur origine.

## Contact

Pour toute demande d'informations, n'hésitez pas à me contacter à : cyril[point]iaconelli[at]ac-rennes.fr
